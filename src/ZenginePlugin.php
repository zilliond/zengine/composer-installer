<?php

namespace Zengine\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class ZenginePlugin implements PluginInterface
{
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new ZengineInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}